package routers

import (
	"Cloud/middleware/jwt"
	"Cloud/pkg/consts"
	"Cloud/pkg/setting"
	"Cloud/routers/api"
	"github.com/gin-gonic/gin"
)

func InitROuter() *gin.Engine {
	r := gin.New()

	r.Use(gin.Logger())

	r.Use(gin.Recovery())

	gin.SetMode(setting.RunMode)

	r.POST("/api/login", api.GetAuth)

	r.GET("/api/ssh", api.Ssh)

	r.POST("/createuser", api.CreateUser)

	r.POST("/api/editUser",jwt.JWT(consts.AUTH_ADMIN),api.EditUserRS)

	r.GET("/api/userList",jwt.JWT(consts.AUTH_ADMIN),api.GetUserUsage)

	r.GET("/api/podlist/:username",jwt.JWT(consts.AUTH_ADMIN), api.ListPods)

	r.GET("/api/nodeInfo",jwt.JWT(consts.AUTH_ADMIN),api.GetNodeInfo)

	r.POST("/api/delete/:username",jwt.JWT(consts.AUTH_ADMIN), api.Delete)

	r.POST("/api/podlog/:username",jwt.JWT(consts.AUTH_ADMIN), api.GetPodLog)

	//r.Use(jwt.JWT(consts.AUTH_TEACHER))
	//{
	//
	//}

	r.POST("/api/podlog",jwt.JWT(consts.AUTH_USER), api.GetPodLog)

	r.GET("/api/podlist",jwt.JWT(consts.AUTH_USER), api.ListPods)

	r.POST("/api/create",jwt.JWT(consts.AUTH_USER), api.Create)

	r.POST("/api/delete",jwt.JWT(consts.AUTH_USER), api.Delete)

	r.GET("/api/images",jwt.JWT(consts.AUTH_USER), api.GetRepos)

	r.POST("/api/validateName",jwt.JWT(consts.AUTH_USER), api.IsNameExist)

	r.GET("/api/currentUser",jwt.JWT(consts.AUTH_USER), api.GetUserInfo)

	r.POST("/api/validateOldPassword",jwt.JWT(consts.AUTH_USER), api.CheckPassword)

	r.POST("/api/updateUserInfo",jwt.JWT(consts.AUTH_USER), api.UpdateUserInfo)

	return r
}
