package api

import (
	"Cloud/models"
	"Cloud/pkg/consts"
	"Cloud/pkg/util"
	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
	"net/http"
)

type auth struct {
	Username string `valid:"Required; MaxSize(50)"`
	Password string `valid:"Required; MaxSize(50)"`
}

func GetAuth(c *gin.Context) {
	username := c.Request.FormValue("username")
	password := c.Request.FormValue("password")

	valid := validation.Validation{}
	a := auth{Username: username, Password: password}
	ok, _ := valid.Valid(&a)

	data := make(map[string]interface{})
	code := consts.INVALID_PARAMS
	var isExist int
	if ok {
		isExist = models.CheckAuth(username, password)
		if isExist > 0 {
			token, err := util.GenerateToken(username, password,isExist)
			if err != nil {
				code = consts.ERROR_AUTH_TOKEN
			} else {
				data["token"] = token
				data["currentAuthority"] = consts.GetMsg(isExist)

				code = consts.SUCCESS
			}

		} else {
			code = consts.ERROR_AUTH
		}
	}

	c.JSON(http.StatusOK, gin.H{
		"code" : code,
		"msg" :  consts.GetMsg(code),
		"data" : data,
	})
}
