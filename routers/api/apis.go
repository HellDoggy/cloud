package api

import (
	"Cloud/pkg/consts"
	"Cloud/pkg/util"
	"Cloud/pkg/util/ws"
	"bytes"
	"encoding/base64"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"io"
	"io/ioutil"
	"k8s.io/api/apps/v1beta1"
	v12 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	meta_v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/client-go/kubernetes/scheme"
	v1beta12 "k8s.io/client-go/kubernetes/typed/apps/v1beta1"
	v1 "k8s.io/client-go/kubernetes/typed/core/v1"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/remotecommand"
	"net/http"
	"os"
	"sort"
	"strings"
	"time"
)

const (
	gob_path = consts.PATH + "gob/nodes.info"
)

var(
	deployment = v1beta1.Deployment{}

	service = v12.Service{}

	YAML []byte
	JSON []byte

	err error
	code int
	logger *zap.Logger
)

type Node struct{
	Node		string		`json:"name"`
	Usedmem		int64		`json:"usedMem"`
	Totmem		int64		`json:"totalMem"`
	UsedCPU		int64		`json:"usedCPU"`
	TotCPU		int64		`json:"totalCPU"`
}

type ChartData struct {
	Usedmem		int64		`json:"y1"`
	UsedCPU		int64		`json:"y2"`
	Timestamp	int64		`json:"x"`
}

type Nodeinfo struct {
	Node		string		`json:"name"`
	Totmem		int64		`json:"totalMem"`
	TotCPU		int64		`json:"totalCPU"`
	ChartData	[]ChartData	`json:"chartData"`
}

type Nodeinfos []Nodeinfo

type form struct {
	Name 		string 				`json:"name"`
	Podname		string				`json:"podname"`
	Linenum		int	   				`json:"linenum"`
	Image		[]string			`json:"image"`
	Env			[]Children			`json:"env"`
	Port 		[]int				`json:"port"`
	Limit		map[string]int64	`json:"limit"`
}

type deleteForm struct {
	Name 		[]string 	`json:"podname"`
}

type podinfo struct{
	Name 		string		`json:"name"`
	Podname 	string 		`json:"podname"`
	Image 		string 		`json:"image"`
	Status 		string 		`json:"status"`
	Addr		string 		`json:"address"`
	Port 		[]string 	`json:"port"`
	Createtime	int64  		`json:"createTime"`
	UsedMem		int64		`json:"usedMem"`
	UsedCPU		int64		`json:"usedCPU"`
}

type repository struct {
	Names []string `json:"repositories"`
}

type svcversion struct{
	Name	string 		`json:"name"`
	Tags	[]string	`json:"tags"`
}

type Children struct {
	Value string `json:"value"`
	Label string `json:"label"`
}

type repoinfo struct {
	Childs	[]Children `json:"children"`
	Value	string `json:"value"`
	Label   string `json:"label"`
}

type resourceinfo struct{
	Requestcpu	int64		`json:"requestCPU"`
	Requestmem	int64		`json:"requestMem"`
	Requeststo	int64		`json:"requestStorage"`
}

func init(){
	logger = util.GetLogger()

}

func checkToken(c *gin.Context)*util.Claims{
	code = consts.SUCCESS
	var claims *util.Claims
	token := c.Request.Header.Get("Authorization")
	if token == ""{
		code = consts.ERROR_AUTH_TOKEN
		logger.Error("token为空")
		return nil
	}else {
		token = token[7:]
		claims,err = util.ParseToken(token)
		if err != nil{
			code = consts.ERROR_AUTH_TOKEN
			logger.Error("token无效")
			return nil
		}
	}
	return claims
}

func createDeploy(username string,deployItf v1beta12.DeploymentInterface, name string,repo string,env []Children,limit map[string]int64){
	code = consts.SUCCESS
	var replicas int32
	var container v12.Container
	var envs []v12.EnvVar
	deployment.APIVersion = "extensions/v1beta1"
	deployment.Kind = "Deployment"
	deployment.SetName(name)
	replicas = 1
	tmp := make(map[string]string)
	tmp["app"] = name
	container.Image = consts.HOST + ":5000/" + repo
	container.Name = name

	var requestcpu,requestmemory,requeststorage float64
	 flag := 0
	if limit != nil && len(limit) != 0{
		for k,v:=range(limit){
			switch k {
			case "requestCPU" :
				flag = flag | 1
				requestcpu = float64(v)
			case "requestMem":
				flag = flag | 2
				requestmemory = float64(v)
			case "requestStorage":
				flag = flag | 4
				requeststorage = float64(v)
			}
		}
		usage := getResourceUsage(username)
		used := usage["used"]
		total := usage["total"]
		rlist := make(map[v12.ResourceName]resource.Quantity)
		llist := make(map[v12.ResourceName]resource.Quantity)
		if flag & 1 >0{
			usedCPU := used.Requestcpu
			totalCPU := total.Requestcpu
			if usedCPU + int64(requestcpu) > totalCPU{
				code = consts.ERROR_RESOURCE_OUT
				return
			}
			rlist[v12.ResourceCPU] = resource.MustParse(fmt.Sprint(requestcpu) + "m")
			llist[v12.ResourceCPU ] = resource.MustParse(fmt.Sprint(requestcpu*1.2) + "m")
		}
		if flag & 2 > 0{
			usedMem := used.Requestmem
			totalMem := total.Requestmem
			if usedMem + int64(requestmemory) > totalMem{
				code = consts.ERROR_RESOURCE_OUT
				return
			}
			rlist[v12.ResourceMemory] = resource.MustParse(fmt.Sprint(requestmemory)+"M")
			llist[v12.ResourceMemory] = resource.MustParse(fmt.Sprint(requestmemory*1.2)+"M")
		}
		if flag & 4 > 0{
			usedSto := used.Requeststo
			totalSto := total.Requeststo
			if usedSto + int64(requeststorage) > totalSto{
				code = consts.ERROR_RESOURCE_OUT
				return
			}
			rlist[v12.ResourceEphemeralStorage] = resource.MustParse(fmt.Sprint(requeststorage)+"M")
			llist[v12.ResourceEphemeralStorage] = resource.MustParse(fmt.Sprint(requeststorage*1.2)+"M")
		}
		container.Resources = v12.ResourceRequirements{
			Requests: rlist,
			Limits: llist,
		}
	}
	for i := 0;i < len(env);i++{
		var envvar v12.EnvVar
		envvar.Name = env[i].Label
		envvar.Value = env[i].Value
		envs = append(envs,envvar)
	}
	container.Env = envs
	deploySpec := v1beta1.DeploymentSpec{
		Replicas: &replicas,
		Template: v12.PodTemplateSpec{
			ObjectMeta: meta_v1.ObjectMeta{
				Labels: tmp,
			},
			Spec: v12.PodSpec{
				Containers: []v12.Container{container},
			},
		},
	}
	deployment.Spec = deploySpec
	if _,err = deployItf.Get(deployment.Name,meta_v1.GetOptions{});err != nil{

		if _,err = deployItf.Create(&deployment); err != nil {
			code = consts.ERROR_DEPLOY_CREATE
			return
		}
	}else {
		code = consts.ERROR_DEPLOY_ALREADY_EXIST
		return
	}
	logger.Info("创建deployment成功")
	return
}

func deleteDeploy(deployItf v1beta12.DeploymentInterface,name string){

	code = consts.SUCCESS
	deletePlicy := meta_v1.DeletePropagationForeground
	err = deployItf.Delete(name,&meta_v1.DeleteOptions{
		PropagationPolicy: &deletePlicy,
	})
	if err != nil{
		code = consts.ERROR_DEPLOY_NOT_EXIST
		return
	}
	logger.Info("删除deployment成功")
	return
}

func createSvc(svcItf v1.ServiceInterface,name string,repo string,port []int){
	tmp := make(map[string]string)
	tmp["app"] = name
	//svc := repo[:strings.Index(repo,":")]
	//port := consts.Getport(svc)
	svcport := []v12.ServicePort{}
	for i:=0 ; i < len(port) ; i++{
		porti := port[i]
		svcport=append(svcport,v12.ServicePort{Port: int32(porti),TargetPort: intstr.FromInt(porti),Name: fmt.Sprint(porti)})
	}
	service.APIVersion = "v1"
	service.Kind = "Service"
	service.SetName(name)
	service.Spec = v12.ServiceSpec{
		Type: v12.ServiceTypeNodePort,
		Ports: svcport,
		Selector: tmp,
	}
	if _,err = svcItf.Get(service.Name,meta_v1.GetOptions{});err != nil{
		if _,err = svcItf.Create(&service); err != nil {
			code = consts.ERROR_SERVICE_CREATE
			return
		}
	}else {
		code = consts.ERROR_SERVICE_ALREADY_EXIST
		return
	}
	logger.Info("创建service成功")
	return
}

func deleteSvc(svcItf v1.ServiceInterface,name string){
	code = consts.SUCCESS
	err = svcItf.Delete(name,&meta_v1.DeleteOptions{})
	if err != nil{
		code = consts.ERROR_SERVICE_NOT_EXIST
		return
	}
	logger.Info("删除service成功")
	return
}

func Create(c *gin.Context){
	var data bool
	claims := checkToken(c)
	if code != consts.SUCCESS{
		goto LAST
	}else{
		username := claims.Username

		var forminfo form
		_ = c.BindJSON(&forminfo)
		name := forminfo.Name
		imagepiece := forminfo.Image
		limit := forminfo.Limit
		env := forminfo.Env
		port := forminfo.Port
		image := imagepiece[0] + ":" + imagepiece[1]

		name = name + "-" + username

		deployItf := util.GetDeployItf(username)
		svcItf := util.GetSvcItf(username)
		createDeploy(username,deployItf,name,image,env,limit)
		if code != consts.SUCCESS{
			logger.Error("创建deployment失败",zap.String("reason",consts.GetMsg(code)))
			data = false
			goto LAST
		}
		createSvc(svcItf,name,image,port)
		if code != consts.SUCCESS{
			logger.Error("创建service失败",zap.String("reason",consts.GetMsg(code)))
			data = false
			deleteDeploy(deployItf,name)
			goto LAST
		}
		logger.Info("创建容器成功")
		data = true
	}
LAST:
	//c.JSON(http.StatusOK, gin.H{
	//	"code" : code,
	//	"msg" :  consts.GetMsg(code),
	//	"data" : data,
	//})
	c.Set("code",code)
	c.Set("msg",consts.GetMsg(code))
	c.Set("data",data)
	return
}

func Delete(c *gin.Context){
	claims := checkToken(c)
	if code != consts.SUCCESS {
		goto LAST
	}else{
		username := claims.Username
		username2 := c.Param("username")
		if username2 != ""{
			username = username2
		}
		deployItf := util.GetDeployItf(username)
		var delForm deleteForm
		_ = c.BindJSON(&delForm)
		names := delForm.Name

		svcItf := util.GetSvcItf(username)
		podItf :=util.GetPodItf(username)

		for i := 0 ; i < len(names);i++{
			podname := names[i]
			pod,err := podItf.Get(podname,meta_v1.GetOptions{})
			if err != nil{
				logger.Error("删除pod失败",zap.String("reason","pod不存在"))
				continue
			}
			if pod.Status.Phase == v12.PodFailed{
				if pod.Status.Reason == "Evicted"{
					podItf.Delete(podname,&meta_v1.DeleteOptions{})
					continue
				}
			}
			name := podname[:strings.LastIndex(podname,username)] + username
			
			deleteDeploy(deployItf,name)
			if code != consts.SUCCESS{
				logger.Error("删除deployment失败",zap.String("reason",consts.GetMsg(code)))
				goto LAST
			}
			deleteSvc(svcItf,name)
			if code != consts.SUCCESS{
				logger.Error("删除service失败",zap.String("reason",consts.GetMsg(code)))
				goto LAST
			}
			logger.Info("删除容器成功")
		}
	}
LAST:
	ListPods(c)
	return
}

func ListPods(c *gin.Context){
	var pods []podinfo
	var quota map[string]resourceinfo
	var data struct{
		Username 	string					`json:"username"`
		UsedMem		int64					`json:"usedMem"`
		UsedCPU		int64					`json:"usedCPU"`
		Pods	[]podinfo 					`json:"pods"`
		Quota	map[string]resourceinfo		`json:"quota"`
	}
	claims := checkToken(c)
	if code != consts.SUCCESS{
		goto LAST
	}else{
		username := claims.Username

		username2 := c.Param("username")
		fmt.Println(username2)
		if username2 != ""{
			username = username2
			//username1,err := base64.StdEncoding.DecodeString(username2)
			//if err != nil{
			//	logger.Error("username解码失败")
			//	code = consts.ERROR
			//	goto LAST
			//}
			//username = string(username1)
		}

		podItf := util.GetPodItf(username)
		svcItf := util.GetSvcItf(username)

		var podinfo1 podinfo
		data.Username = username
		if podList, err := podItf.List(meta_v1.ListOptions{}); err == nil {
			for _, pod := range podList.Items {
				podName := pod.Name
				podStatus := string(pod.Status.Phase)
				idx := strings.LastIndex(podName,username)
				svcname := podName[:idx] + username
				svc,err := svcItf.Get(svcname,meta_v1.GetOptions{})
				podinfo1.Name = svcname
				podinfo1.Podname = podName
				podinfo1.Image = pod.Spec.Containers[0].Image
				podinfo1.Image = podinfo1.Image[strings.Index(podinfo1.Image,"/")+1:]
				podinfo1.Createtime = pod.CreationTimestamp.Unix()
				podinfo1.Addr = consts.HOST

				var podmetrics util.PodMetrics
				_ = util.GetPodMetric(util.GetClientset(),&podmetrics,username,podName)
				for _,item := range podmetrics.Containers{
					cpu := resource.MustParse(item.Usage.CPU)
					mem := resource.MustParse(item.Usage.Memory)
					podinfo1.UsedCPU += cpu.MilliValue()
					podinfo1.UsedMem += mem.ScaledValue(resource.Mega)
					data.UsedMem += podinfo1.UsedMem
					data.UsedCPU += podinfo1.UsedCPU
				}

				if svc != nil && err == nil{
					size := len(svc.Spec.Ports)
					ports := []string{}
					svcports := svc.Spec.Ports
					for i:=0 ; i < size ; i++{
						ports = append(ports,fmt.Sprint(svcports[i].Port) + ":" + fmt.Sprint(svcports[i].NodePort))
					}
					podinfo1.Port = ports
				}else {
					podinfo1.Port = nil
					podStatus = "Terminating"
					goto KO
				}

				//存储超标
				if pod.Status.Phase == v12.PodFailed{
					podStatus = pod.Status.Reason
					goto KO
				}

				//内存超标
				if len(pod.Status.ContainerStatuses) != 0{
					for i:=0; i<len(pod.Status.ContainerStatuses);i++{
						cs := pod.Status.ContainerStatuses[i]
						if &cs.LastTerminationState != nil{
							if cs.LastTerminationState.Terminated != nil{
								podStatus = cs.LastTerminationState.Terminated.Reason
								goto KO
							}
						}
					}
				}

				// PodRunning means the pod has been bound to a node and all of the containers have been started.
				// At least one container is still running or is in the process of being restarted.
				//if podStatus != string(v12.PodRunning) {
				//	// 汇总错误原因不为空
				//	if pod.Status.Reason != "" {
				//		podStatus = pod.Status.Reason
				//		goto KO
				//	}
				//
				//	// condition有错误信息
				//	for _, cond := range pod.Status.Conditions {
				//		if cond.Type == v12.PodReady {	// POD就绪状态
				//			if cond.Status != v12.ConditionTrue {	// 失败
				//				podStatus = cond.Reason
				//			}
				//			goto KO
				//		}
				//	}
				//
				//	// 没有ready condition, 状态未知
				//	podStatus = "Unknown"
				//}
			KO:
				podinfo1.Status = podStatus
				pods = append(pods, podinfo1)
			}
			quota = getResourceUsage(username)
			data.Pods = pods
			data.Quota = quota
		}
	}
LAST:
	c.Set("code",code)
	c.Set("msg",consts.GetMsg(code))
	c.Set("data",data)
	return

}

func GetPodLog(c *gin.Context){
	var data string
	claims := checkToken(c)
	if code != consts.SUCCESS {
		goto LAST
	}else{
		username := claims.Username
		username2 := c.Param("username")
		if username2 != ""{
			username = username2
		}
		podItf := util.GetPodItf(username)
		var forminfo form
		_ = c.BindJSON(&forminfo)
		name := forminfo.Podname
		linenum := forminfo.Linenum
		if linenum < 0 {
			linenum = 0
		}

		deployItf := util.GetPodItf(username)

		if _,err = deployItf.Get(name,meta_v1.GetOptions{});err != nil{
			code = consts.ERROR_POD_NOT_EXIST
			goto LAST
		}

		req := podItf.GetLogs(name,&v12.PodLogOptions{})
		podLogs, _ := req.Stream()

		defer podLogs.Close()

		buf := new(bytes.Buffer)
		_, err = io.Copy(buf, podLogs)

		data = buf.String()

		switch linenum {
		case 0:
			goto LAST
		default:
			datatmp := ""
			if data == ""{
				goto LAST
			}
			data = data[:len(data)-1]
			for ;linenum > 0;linenum -- {
				idx := strings.LastIndex(data,"\n")
				if idx < 0{
					datatmp = data + datatmp
					break;
				}
				datatmp = data[idx:] + datatmp
				data = data[:idx]
			}
			data = datatmp
			if data[0] == '\n'{
				data = data[1:]
			}
			data = data + "\n"
		}
	}
LAST:
	c.Set("code",code)
	c.Set("msg",consts.GetMsg(code))
	c.Set("data",data)
	return
}

func GetRepos(c *gin.Context){
	code = consts.SUCCESS
	client := &http.Client{}
	resp, _ := client.Get("http://10.251.0.14:5000/v2/_catalog")
	defer resp.Body.Close()
	body,_ := ioutil.ReadAll(resp.Body)
	var repos repository
	var repoinfos []repoinfo
	err := json.Unmarshal(body,&repos)
	if err != nil{
		code = consts.ERROR
		goto LAST
	}
	for i:=0;i<len(repos.Names);i++{
		reponame := repos.Names[i]
		resp2,_ := client.Get("http://10.251.0.14:5000/v2/" + reponame +"/tags/list")
		defer resp2.Body.Close()
		var svcvers svcversion
		body2,_ := ioutil.ReadAll(resp2.Body)
		err := json.Unmarshal(body2,&svcvers)
		if err != nil{
			code = consts.ERROR
			goto LAST
		}
		var repo repoinfo
		for j:=0;j<len(svcvers.Tags);j++{
			var child Children
			child.Value  = svcvers.Tags[j]
			child.Label = child.Value
			repo.Childs = append(repo.Childs,child)
		}
		repo.Value = svcvers.Name
		repo.Label = repo.Value
		repoinfos = append(repoinfos,repo)
	}
LAST:
	c.Set("code",code)
	c.Set("msg",consts.GetMsg(code))
	c.Set("data",repoinfos)
}

func IsNameExist(c *gin.Context){
	var data bool
	claims := checkToken(c)
	if code != consts.SUCCESS {
		goto LAST
	}else{
		username := claims.Username
		deployItf := util.GetDeployItf(username)
		var forminfo form
		_ = c.BindJSON(&forminfo)
		name := forminfo.Name

		name = name + "-" + username

		fmt.Println(name)

		if _,err = deployItf.Get(name,meta_v1.GetOptions{});err == nil{
			data = true
		}else{
			data = false
		}
	}
LAST:
	c.Set("code",code)
	c.Set("msg",consts.GetMsg(code))
	c.Set("data",data)
	return
}

func getResourceUsage(username string)map[string]resourceinfo{
	data := make(map[string]resourceinfo)
	rqItf := util.GetClientset().CoreV1().ResourceQuotas(username)
	rq,err := rqItf.Get("resource-"+username,meta_v1.GetOptions{})
	if err != nil{
		logger.Error("获取命名空间资源使用情况失败")
		code = consts.ERROR
		return nil
	}

	hardrcpu := rq.Status.Hard[v12.ResourceRequestsCPU]
	hardrmem := rq.Status.Hard[v12.ResourceRequestsMemory]
	hardrsto := rq.Status.Hard[v12.ResourceRequestsEphemeralStorage]

	usedrcpu := rq.Status.Used[v12.ResourceRequestsCPU]
	usedrmem := rq.Status.Used[v12.ResourceRequestsMemory]
	usedrsto := rq.Status.Used[v12.ResourceRequestsEphemeralStorage]

	var rshard,rsused resourceinfo
	rshard.Requestcpu = hardrcpu.MilliValue()
	rshard.Requestmem = hardrmem.ScaledValue(resource.Mega)
	rshard.Requeststo = hardrsto.ScaledValue(resource.Mega)

	rsused.Requestcpu = usedrcpu.MilliValue()
	rsused.Requestmem = usedrmem.ScaledValue(resource.Mega)
	rsused.Requeststo = usedrsto.ScaledValue(resource.Mega)

	data["total"] = rshard
	data["used"] = rsused

	return data
}

func Ssh(c *gin.Context){
	var (
		wsConn *ws.WsConnection
		restConf *rest.Config
		sshReq *rest.Request
		podName string
		containerName string
		executor remotecommand.Executor
		handler *util.StreamHandler
		token string
		data bool
	)
	podName = c.Query("podname")
	podName1,err := base64.StdEncoding.DecodeString(podName)
	if err != nil{
		logger.Error("podname解码失败")
		code = consts.ERROR
		goto LAST
	}
	podName = string(podName1)
	fmt.Println(podName)
	token = c.Query("token")
	code = consts.SUCCESS
	if token == ""{
		code = consts.ERROR_AUTH_TOKEN
		logger.Error("token为空")
		goto LAST
	}else {
		claims,err := util.ParseToken(token)
		if err != nil{
			code = consts.ERROR_AUTH_TOKEN
			logger.Error("token无效")
			goto LAST
		}else {
			username := claims.Username
			username2 := c.Query("username")
			if username2 != ""{
				username = username2
			}
			containerName = podName[:strings.LastIndex(podName,username)] + username

			// 得到websocket长连接
			if wsConn, err = ws.InitWebsocket(c.Writer, c.Request); err != nil {
				return
			}

			sshReq = util.GetClientset().CoreV1().RESTClient().Post().
				Resource("pods").
				Name(podName).
				Namespace(username).
				SubResource("exec").
				VersionedParams(&v12.PodExecOptions{
					Container:	containerName,
					Command: 	[]string{"bash"},
					Stdin: true,
					Stdout: true,
					Stderr: true,
					TTY: true,
				},scheme.ParameterCodec)

			restConf = util.GetRestConfig()

			// 创建到容器的连接
			if executor, err = remotecommand.NewSPDYExecutor(restConf, "POST", sshReq.URL()); err != nil {
				goto LAST
			}

			// 配置与容器之间的数据流处理回调
			handler = &util.StreamHandler{ WsConn: wsConn, ResizeEvent: make(chan remotecommand.TerminalSize)}
			if err = executor.Stream(remotecommand.StreamOptions{
				Stdin:             handler,
				Stdout:            handler,
				Stderr:            handler,
				TerminalSizeQueue: handler,
				Tty:               true,
			}); err != nil {
				goto LAST
			}
			data = true
			return
		}

	}
LAST:
	c.Set("code",code)
	c.Set("msg",consts.GetMsg(code))
	c.Set("data",data)
	wsConn.WsClose()
	return
}

type Sortnode []Node

func (s Sortnode) Less(i,j int) bool{
	return s[i].Node < s[j].Node
}

func (s Sortnode) Len() int{
	return len(s)
}

func (s Sortnode) Swap(i, j int) {s[i],s[j]=s[j],s[i]}

func GetTotalResource(){
	var data Sortnode
	if code != consts.SUCCESS{
		goto LAST
	}else{
		var nodes util.NodeMetricsList

		_ = util.GetNodeMetric(util.GetClientset(),&nodes)
		nodeItf := util.GetNodeItf()

		for _,item := range nodes.Items{

			var nodeinfo Node

			k8sNode,_ := nodeItf.Get(item.Metadata.Name,meta_v1.GetOptions{})
			nodeinfo.Node = item.Metadata.Name
			mem := resource.MustParse(item.Usage.Memory)
			cpu := resource.MustParse(item.Usage.CPU)
			nodeinfo.Usedmem = mem.ScaledValue(resource.Mega)
			nodeinfo.UsedCPU = cpu.MilliValue()

			nodeinfo.Totmem = k8sNode.Status.Allocatable.Memory().ScaledValue(resource.Mega)
			nodeinfo.TotCPU = k8sNode.Status.Allocatable.Cpu().MilliValue()

			data = append(data,nodeinfo)
		}

		sort.Sort(data)
		var tmpnodes Nodeinfos
		err = Read(&tmpnodes)
		if len(tmpnodes) == 0{
			for _,item:= range data{
				var nodeinfo Nodeinfo
				var chartdata ChartData
				nodeinfo.Node = item.Node
				nodeinfo.TotCPU = item.TotCPU
				nodeinfo.Totmem = item.Totmem
				chartdata.UsedCPU = item.UsedCPU
				chartdata.Usedmem = item.Usedmem
				chartdata.Timestamp = time.Now().Unix() * 1000
				nodeinfo.ChartData = append(nodeinfo.ChartData,chartdata)

				tmpnodes = append(tmpnodes,nodeinfo)
			}
		}else{
			for i,item:= range data{
				var chartdata ChartData
				chartdata.UsedCPU = item.UsedCPU
				chartdata.Usedmem = item.Usedmem
				chartdata.Timestamp = time.Now().Unix() * 1000
				tmpnodes[i].ChartData = append(tmpnodes[i].ChartData,chartdata)
				if len(tmpnodes[i].ChartData) > 60{
					tmpnodes[i].ChartData = tmpnodes[i].ChartData[1:]
				}
			}
		}
		err = Write(&tmpnodes)
		if err != nil{
			return
		}
	}
LAST:
	return
}

type Useruse struct{
	Username 	string					`json:"username"`
	UsedMem		int64					`json:"usedMem"`
	UsedCPU		int64					`json:"usedCPU"`
}

func GetUserUsage(c *gin.Context){
	_ = checkToken(c)
	var data []Useruse
	var podlist util.PodMetricsList
	if code != consts.SUCCESS{
		goto LAST
	}else{
		nsItf := util.GetNSItf()
		nslist , _ :=nsItf.List(meta_v1.ListOptions{})
		for _,item := range nslist.Items{
			ns := item.GetName()
			if ns == "default" || strings.Index(ns,"kube") >= 0{
				continue
			}
			var useruse Useruse
			useruse.Username = ns
			_ = util.GetMetrics(util.GetClientset(),&podlist,item.GetName())
			for _,i := range podlist.Items{
				for _,j := range i.Containers{
					cpu := resource.MustParse(j.Usage.CPU)
					mem := resource.MustParse(j.Usage.Memory)
					useruse.UsedCPU += cpu.MilliValue()
					useruse.UsedMem += mem.ScaledValue(resource.Mega)
				}
			}
			data = append(data,useruse)
		}
	}
LAST:
	c.Set("code",code)
	c.Set("msg",consts.GetMsg(code))
	c.Set("data",data)
	return
}

func EditUserRS(c *gin.Context){
	_ = checkToken(c)
	var data bool
	if code != consts.SUCCESS{
		goto LAST
	}else{
		var infos struct{
			Username 	string 		`json:"username"`
			Requestcpu	int64		`json:"requestCPU"`
			Requestmem	int64		`json:"requestMem"`
			Requeststo	int64		`json:"requestStorage"`
		}
		_ = c.Bind(&infos)
		RQItf := util.GetRQItf(infos.Username)
		RQ,err := RQItf.Get("resource-" + infos.Username,meta_v1.GetOptions{})
		if err != nil{
			code = consts.ERROR_RESOURCEQUOTA_GET
			logger.Error(consts.GetMsg(code))
			goto LAST
		}
		RQ.Spec = v12.ResourceQuotaSpec{
			Hard: v12.ResourceList{
				v12.ResourceLimitsCPU : resource.MustParse(fmt.Sprint(int64(float64(infos.Requestcpu) * 1.2)) + "m"),
				v12.ResourceRequestsCPU: resource.MustParse(fmt.Sprint(infos.Requestcpu) + "m"),
				v12.ResourceLimitsMemory : resource.MustParse(fmt.Sprint(int64(float64(infos.Requestmem) * 1.2)) + "M"),
				v12.ResourceRequestsMemory : resource.MustParse(fmt.Sprint(infos.Requestmem) + "M"),
				v12.ResourceLimitsEphemeralStorage: resource.MustParse(fmt.Sprint(int64(float64(infos.Requeststo) * 1.2)) + "M"),
				v12.ResourceRequestsEphemeralStorage: resource.MustParse(fmt.Sprint(infos.Requeststo) + "M"),
			},
		}
		_,err = RQItf.Update(RQ)
		if err != nil{
			code = consts.ERROR_RESOURCEQUOTA_UPDATE
			logger.Error(consts.GetMsg(code))
			goto LAST
		}
		data = true
	}
LAST:
	c.Set("code",code)
	c.Set("msg",consts.GetMsg(code))
	c.Set("data",data)
	return
}

func GetNodeInfo(c *gin.Context){
	_ = checkToken(c)
	var data Nodeinfos
	if code != consts.SUCCESS{
		goto LAST
	}else{
		err = Read(&data)
		if err != nil{
			goto LAST
		}
	}
LAST:
	c.Set("code",code)
	c.Set("msg",consts.GetMsg(code))
	c.Set("data",data)
	return
}


func Read(nodes *Nodeinfos)error{
	file,err := os.Open(gob_path)
	if err != nil{
		logger.Error("打开gob数据出错,文件不存在")
		return err
	}
	dec := gob.NewDecoder(file)
	err2 := dec.Decode(nodes)
	if err2 != nil{
		logger.Error("gob数据编码出错")
		return err2
	}
	return nil
}

func Write(nodes *Nodeinfos) error {
	file, err := os.Create(gob_path)
	if err != nil {
		logger.Error("打开gob数据出错")
		return err
	}
	enc := gob.NewEncoder(file)
	err2 := enc.Encode(nodes)
	if err2 != nil{
		logger.Error("gob数据译码出错")
		return err2
	}
	return nil
}