package api

import (
	"Cloud/models"
	"Cloud/pkg/consts"
	"github.com/gin-gonic/gin"
	"net/http"
)

func CreateUser(c *gin.Context){
	var user struct{
		Username	string		`json:"username"`
		Password	string		`json:"password"`
		Authority	int			`json:"authority"`
	}

	err := c.Bind(&user)
	if err != nil{
		c.String(http.StatusOK,"格式错误")
		return
	}
	models.CreateUser(user.Username,user.Password,user.Authority)
	c.String(http.StatusOK,"创建成功")
}

func GetUserInfo(c *gin.Context){
	data := make(map[string]string)
	claims:=checkToken(c)
	if code != consts.SUCCESS {
		goto LAST
	}else{
		username := claims.Username
		info := models.GetInfo(username)
		data["name"] = info.Name
		data["email"] = info.Email
		data["profile"] = info.Profile
		data["authority"] = consts.GetMsg(info.Authority)
	}
LAST:
	c.Set("code",code)
	c.Set("msg",consts.GetMsg(code))
	c.Set("data",data)
	return
}

func CheckPassword(c *gin.Context){
	claims := checkToken(c)
	var flag bool
	if code != consts.SUCCESS{
		goto LAST
	}else{
		username := claims.Username
		var userinfo struct{
			Password	string		`json:"password"`
		}
		_ = c.Bind(&userinfo)
		flag = models.CheckPassword(username,userinfo.Password)
	}
LAST:
	c.Set("code",code)
	c.Set("msg",consts.GetMsg(code))
	c.Set("data",flag)
	return
}

func UpdateUserInfo(c *gin.Context){
	claims := checkToken(c)
	var data bool
	if code != consts.SUCCESS{
		goto LAST
	}else{
		username := claims.Username
		var userinfo struct{
			Email 		string 		`json:"email"`
			Profile 	string 		`json:"profile"`
			Oldpasswd	string		`json:"oldPassword"`
			Newpasswd	string 		`json:"newPassword"`
		}
		_ = c.Bind(&userinfo)
		if userinfo.Oldpasswd != "" {
			flag := models.CheckPassword(username, userinfo.Oldpasswd)
			if !flag {
				data = false
				code = consts.ERROR
				goto LAST
			}
			models.UpdatePassword(username, userinfo.Newpasswd)
		}else{
			models.UpdateInfo(username, userinfo.Email, userinfo.Profile)
		}
		data = true
	}
LAST:
	c.Set("code",code)
	c.Set("msg",consts.GetMsg(code))
	c.Set("data",data)
	return
}
