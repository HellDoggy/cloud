package consts

var MsgFlags = map[int]string {
	AUTH_USER: "user",
	AUTH_TEACHER: "teacher",
	AUTH_ADMIN: "admin",

	SUCCESS : "ok",
	ERROR : "fail",
	INVALID_PARAMS : "请求参数错误",
	ACCESS_DENIED : "当前权限不够",
	ERROR_EXIST_USER : "已存在该用户",
	ERROR_NOT_EXIST_USER : "该用户不存在",
	ERROR_AUTH_CHECK_TOKEN_FAIL : "Token鉴权失败",
	ERROR_AUTH_CHECK_TOKEN_TIMEOUT : "Token已超时",
	ERROR_AUTH_TOKEN : "Token生成失败",
	ERROR_AUTH : "用户名或密码错误",
	ERROR_AUTH_PERMISSION_DENIED : "用户权限不足",
	ERROR_DEPLOY_CREATE: "deployment创建失败",
	ERROR_DEPLOY_UPDATE: "deployment更新失败",
	ERROR_SERVICE_CREATE: "service创建失败",
	ERROR_SERVICE_UPDATE: "service更新失败",
	ERROR_YAML_READ: "读取yaml文件失败",
	ERROR_YAML_CONVERT: "转换yaml文件失败",
	ERROR_YAML_UNMARSHAL: "yaml数据编出失败",
	ERROR_DEPLOY_ALREADY_EXIST : "deployment已存在",
	ERROR_DEPLOY_NOT_EXIST: "deployment不存在",
	ERROR_SERVICE_ALREADY_EXIST: "service已存在",
	ERROR_SERVICE_NOT_EXIST: "service不存在",
	ERROR_POD_NOT_EXIST: "pod不存在",
	ERROR_RESOURCE_OUT: "用户剩余资源不足",
	ERROR_RESOURCEQUOTA_GET: "用户资源配置获取失败",
	ERROR_RESOURCEQUOTA_UPDATE :"用户资源配置更新失败",
}

func GetMsg(code int) string {
	msg, ok := MsgFlags[code]
	if ok {
		return msg
	}

	return MsgFlags[ERROR]
}