package util

import (
	"Cloud/pkg/consts"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
	"os"
)

var logger *zap.Logger

func init(){
	hook := lumberjack.Logger{
		Filename: 		consts.PATH + "logs/paas.log",
		MaxSize: 		128,
		MaxBackups:		30,
		MaxAge:			7,
		Compress:		false,
	}

	encoderConfig := zapcore.EncoderConfig{
		TimeKey:		"time",
		LevelKey:		"level",
		NameKey:		"logger",
		//CallerKey:		"linenum",
		MessageKey:		"msg",
		StacktraceKey:	"stacktrace",
		LineEnding:		zapcore.DefaultLineEnding,
		EncodeLevel:	zapcore.LowercaseLevelEncoder,
		EncodeTime:		zapcore.ISO8601TimeEncoder,
		EncodeDuration:	zapcore.SecondsDurationEncoder,
		EncodeCaller:	zapcore.FullCallerEncoder,
		EncodeName:		zapcore.FullNameEncoder,
	}

	atomicLevel := zap.NewAtomicLevel()
	atomicLevel.SetLevel(zap.InfoLevel)

	core := zapcore.NewCore(
		zapcore.NewJSONEncoder(encoderConfig),                                           // 编码器配置
		zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout), zapcore.AddSync(&hook)), // 打印到控制台和文件
		atomicLevel,                                                                     // 日志级别
	)

	// 开启开发模式，堆栈跟踪
	caller := zap.AddCaller()
	// 开启文件及行号
	development := zap.Development()
	// 构造日志
	logger = zap.New(core, caller, development)
}

func GetLogger() *zap.Logger{
	return logger
}
