module Cloud

go 1.12

require (
	github.com/astaxie/beego v1.12.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.4.0
	github.com/go-ini/ini v1.46.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/websocket v1.4.0
	github.com/imdario/mergo v0.3.7 // indirect
	github.com/jinzhu/gorm v1.9.10
	github.com/rakyll/statik v0.1.6
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337 // indirect
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.10.0
	golang.org/x/crypto v0.0.0-20190820162420-60c769a6c586
	gopkg.in/ini.v1 v1.46.0 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	k8s.io/api v0.0.0-20190820101039-d651a1528133
	k8s.io/apimachinery v0.0.0-20190820100751-ac02f8882ef6
	k8s.io/client-go v11.0.0+incompatible
	k8s.io/utils v0.0.0-20190809000727-6c36bc71fc4a // indirect
)
